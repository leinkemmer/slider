from setuptools import setup

setup(name='slider',
    version='0.1',
    description='A simple slide generation tool with a markdown like syntax',
    author='Lukas Einkemmer',
    author_email='lukas.einkemmer@gmail.com',
    packages=['slider_util'],
    scripts=['slider'],
    include_package_data=True,
    zip_safe=False)

