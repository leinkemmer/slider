# Slider

Slider is a program to generate presentations (in the pdf format) from source files based on a markdown like syntax. Internally, Slider uses LaTeX Beamer to generate the output pdf file and thus can, to supplement the markdown syntax, make use of LaTeX formulas (and virtually any other LaTeX command, if necessary).

**Why not use LaTex Beamer directly?** LaTeX syntax is quite verbose. Thus, in making presentations often more boilerplate code than actual content is necessary. That also makes reading and modifying the source difficult. In addition, presentations usually require only a limited number of elements (bullet points, pictures, highlighting) that can be represented in a much more human friendly form.

**Why not use Pandoc to generate LaTex Beamer?** Pandoc lacks many of the additional features that are required to turn Markdown into a viable alternative for making presentation. For example, better control over how pictures are placed, a way to automatically insert appropriate spacing on the slide, possibility to start an enumeration immediately after a line of text, etc.

## Usage

Install slider using

    git clone https://bitbucket.org/leinkemmer/slider
    cd slider
    sudo python3 setup.py install

Then just run slider on a file (test.md in this case)

    $ slider test.md

This results in a pdf called test.pdf. In addition, test.tex is generated, which contains the LaTeX code used to build the pdf. This file is overwritten on the next run of 'slider' and we do not have to worry about it (it is mostly there for debugging).

## Syntax example 

The source file starts with providing information for the title slide (all entries are optional)

    title:  Some fancy title
    author: Lukas Einkemmer
    institution: University of Innsbruck
    workshop:    Some fancy conference
    jointwork:   people that should be acknowledge
    preamble:    will be inserted into the preamble of the LaTeX document (for usepackage, ...)

Then each slide looks like

    ## Some slide title
        
    An **important** conclusion is
    * A is better than B
    * C is not even worth implementing

    Plots
    ![w=0.5](images/image.png)
    
    \ref: Some fancy paper published in some fancy journal
    \ref: Some other fancy paper

This creates a slide separated into three parts (parts or blocks are started by a blank line). The first block includes an enumeration, the second an image, and the third contains the references. The program takes care of distributing the empty space available appropriately.

Different elements (text, images, ...) can be put side by side using the column statement.

    ## Other title

    \col[w=0.4]
    We see in the figure that
    * A is true
    * B is not true
    \col[w=0.6]
    ![](plot.pdf)
    \endcol

    Some text that spans the entire width.


## Available commands

**Highlighting:** \*\*bold\*\* results in bold text (as in Markdown).

**Enumeration:**  All lines which start with \* form an enumeration (as in Markdown).

**Images:** The command \!\[\]\(image.png\) inserts an image. Multiple images can be put side by side by using \!\[\]\(image1.png,image2.pdf,image3.pdf\). The width or the height of the image (as fraction of the total width/height) can be specified by using ![w=0.5](image.png) and ![h=0.3](image.png), respectively. A specific part of the image can be selected by specifying a file name of the form '\[left right lower upper\]test.pdf'. Coordinates start at the lower left and units of cm are used. Using \!\[box\]\(test.png\) a box is drawn around the image.

**Code:** A line starting with \\\\code starts a code block that ends once a line starting with \\\\endcode is encountered.

**Columns:** Columns can be used to put text or other elements side by side. A column is started by \\col\[w=0.4\](in this case 40% of the width is used for the column). After the content of the first column \\col[w=0.6] starts the second column, and so on. The command \\endcol terminates the current column layout.

**References:** A line starting with '\\ref: Some fancy reference' adds a reference block. Multiple references are put on separate lines.

**Colors:** The LaTex commands \\re (red), \\bl (blue), \\gr (green), \\ora (orange), and \\vi (violet) are defined. They should correspond to colors that are legible even on bad projectors. 
**Comments:** Lines starting with % are treated as comments.

**Sections:** A line starting with a single # will generate a slide that starts the section.

