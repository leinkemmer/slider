"""
A simple slide generation tool with a markdown like syntax. This program generates a .tex file that can be build using Latex beamer (requires the beamerthemeeinkemmer.sty).
"""

import re
import sys, os
from enum import Enum

r_comment = r'(?<!\\)(?:\\\\)*%'

def remove_comments(s):
    return re.split(r_comment, s)[0]

def starts_verbatim(str):
    if r_listing.match(str):
        return True
    else:
        return False

def ends_verbatim(str):
    if r_end.match(str):
        return True
    else:
        return False

class IStream:
    def __init__(self, file_name):
        with open(file_name, 'r') as fs:
            content = fs.read()
        lines = content.split('\n')
        self.lines = []
        inside_verbatim = False
        for line in lines:
            # comments are only removed if we are inside a verbatim section.
            # at the moment this is only the case for listings.
            if ends_verbatim(line):
                inside_verbatim = False
            line_nc = remove_comments(line) if not inside_verbatim else line
            if starts_verbatim(line):
                inside_verbatim = True
            if len(line_nc.strip()) != 0 or len(line.strip())==0:
                self.lines.append(line_nc)
        self.index = 0

    def peek(self):
        return self.lines[self.index]

    def read(self):
        self.index += 1
        return self.lines[self.index-1]

    def eof(self):
        return self.index >= len(self.lines)

def indent(str):
    import textwrap
    return textwrap.indent(str, 4*' ')


r_bold = [ re.compile(r'(.*?)\*{2}(.+?)\*{2}(?!\*)(.*)',re.DOTALL),
           r'\1\\textbf{\2}\3' ]

def process(text,regex,repl):
    text_old = ''
    while text_old != text:
        text_old = text
        text = regex.sub(repl,text)
    return text

def format_text(text):
    for regex,repl in [r_bold]:
        text = process(text,regex,repl)
    return text


def parse_text(fs):
    print(indent('starting text'))
    return fs.read();

r_itemize = re.compile('^\*(?!\*)(.*)')

def parse_itemize(fs):
    print(indent('starting itemize'))
    str = '\\begin{itemize}\n'
    while not fs.eof():
        line = fs.peek()
        m = r_itemize.match(line)
        if m:
            fs.read()
            str += indent('\\item ' + m.group(1) + '\n')
        else:
            break

    return str + '\\end{itemize}\n'

r_listing = re.compile('^\\\\code(\[(.*?)\])?(.*)')
r_end     = re.compile('^\\\\endcode(.*)')

def parse_listing(fs):
    print(indent('starting listing'))
    m = r_listing.match(fs.read().strip())
    lang = 'cpp' if m.group(2)==None else m.group(2)
    str = '\\begin{{minted}}[fontsize=\\small]{{{}}}\n'.format(lang)
    while not fs.eof():
        line = fs.read()
        if r_end.match(line):
            break
        else:
            str += line + '\n'

    return str + '\\end{minted}\n'

r_image_opt   = re.compile(r'\s*?\!\[(.*?)\]\((.*)\)')

def escape_fn(fn):
    # The graphicx package is not able to interpret filenames such as test.a.pdf.
    # Instead one has to use {test.a}.pdf.
    name, ext = os.path.splitext(fn)
    return '{{{0}}}{1}'.format(name,ext)

def image_str(fn, opts, col_size):
    options = []
    if 'w' in opts.keys(): options.append('width={0}\\textwidth'.format(opts['w']))
    if 'h' in opts.keys(): options.append('height={0}\\textheight'.format(opts['h']))
    if len(options)==0:
        options.append('width=1.0\\textwidth')

    # A bounding box specifier in front of the filename in the form [x1,x2,y1,y2] is
    # parsed here.
    bb=''
    print('FN: ', fn)
    if fn[0]=='[':
        closing_idx = fn.find(']')
        str_bb = fn[1:closing_idx]
        print('HERE: ', fn, str_bb, str_bb.split(' '))
        fn = fn[(closing_idx+1):]
        bb = ',bb={{{0}cm {2}cm {1}cm {3}cm}},clip'.format(*str_bb.split(' '))
        print(fn, "--", bb)

    print('opts: ', opts)
    if 'box' in opts.keys():
        return '\\fbox{{ \\includegraphics[{0}{1}]{{{2}}} }}'.format(','.join(options), bb, escape_fn(fn))
    else:
        return '\\includegraphics[{0}{1}]{{{2}}}'.format(','.join(options), bb, escape_fn(fn))

class image_grid:
    def __init__(self, cols):
        self.col_size = 1./float(cols)
        self.cols = cols
        self.idx = 0
        self.str = '\\begin{center}\n'

    def add_image(self, fn, opts):
        col_idx = self.idx%self.cols
        if col_idx == 0:
            self.str += '\\begin{columns}[c]\n'
        self.str += '\\column{{{0}\\textwidth}}\n\\centering\n'.format(self.col_size)
        self.str += image_str(fn, opts, self.col_size) + '\n'
        if col_idx == self.cols-1:
            self.str += '\\end{columns}\n'
        self.idx += 1

    def to_str(self):
        return self.str + '\\end{center}\n'


def parse_opts(str):
    def split(x):
        if '=' in x:
            return tuple(x.split('='))
        else:
            return (x,'')

    if str=='': return {}
    lopts = [split(x) for x in str.split(',')]
    return {key.strip(): value.strip() for (key, value) in lopts}

def parse_image(fs):
    line = fs.read()
    str = ''
    m_g = r_image_opt.match(line)
    if m_g:
        str += ''
        # parse options given inside []
        opts = parse_opts(m_g.group(1))
        # determine the filenames given inside ()
        fns = m_g.group(2).split(',')

        cols = int(opts['c']) if 'c' in opts.keys() else len(fns)
        ig = image_grid(cols)
        for fn in fns:
            ig.add_image(fn.strip(), opts)
        str += ig.to_str()
    else:
        print('WARNING image line could not be matched: ', line)

    return str

r_ref = re.compile('^\\\\ref:(.*)')

def parse_ref(fs):
    lst = []
    while not fs.eof():
        line = fs.peek()
        print('---- ',line)
        m = r_ref.match(line)
        if m:
            fs.read()
            lst.append(m.group(1))
        else:
            break
    return '\\references{' + '\\newline\n'.join(lst) + '}'

r_column = re.compile(r'(\\col)\[(.*?)\]|(\\endcol)')

class ColumnState(Enum):
    INACTIVE=1
    ACTIVE=2

def parse_column(fs, state):
    m = r_column.match(fs.read().strip())
    if m.group(1) == '\\col':
        width = m.group(2)
        if state == ColumnState.INACTIVE:
            return '\\begin{columns}[onlytextwidth]\n\\begin{column}{' + width + '\\textwidth}', ColumnState.ACTIVE
        else:
            return '\\end{column}\n\\begin{column}{' + width + '\\textwidth}', ColumnState.ACTIVE
    elif m.group(0) == '\\endcol':
        return '\\end{column}\n\\end{columns}', ColumnState.INACTIVE


block_template = '\\np\n{}'

def parse_block(fs):
    print(indent('starting block'))
    str = '';
    is_ref = False
    column_state = ColumnState.INACTIVE
    while not fs.eof():
        line = fs.peek()
        # if empty line return from block
        if line.strip() == '':
            fs.read()
            break
        # bullet points
        elif r_itemize.match(line):
            str += format_text(indent(parse_itemize(fs)))
        # program listings (code)
        elif r_listing.match(line):
            str += parse_listing(fs)
        # image
        elif r_image_opt.match(line):
            str += indent(parse_image(fs))
        # reference
        elif r_ref.match(line):
            str += format_text(indent(parse_ref(fs)))
            is_ref = True
        # column layout
        elif r_column.match(line.strip()):
            _str,column_state = parse_column(fs, column_state)
            str += format_text(indent(_str))
        # regular text
        else: 
            str += format_text(indent(parse_text(fs)) + '\n')

    if not str.endswith('\n'): str += '\n'
    return block_template.format(str), is_ref


frame_template = '\n\\begin{{frame}}[fragile]{{{0}}}\n{1}\n\\end{{frame}}\n'

def parse_slide(fs, title):
    print('Slide with title: ', title)
    str = ''
    is_ref = False
    while not fs.eof():
        line = fs.peek()
        # if new slide starts return from function
        if r_slide.match(line) or r_section.match(line):
            break

        # if not empty we start a new block
        if line.strip() != '':
            _str,is_ref = parse_block(fs)
            str += _str
        else:
            fs.read()

    if is_ref:
        return frame_template.format(title, str)
    else:
        return frame_template.format(title, str + indent('\\np'))


def replace_file_ext(fn, extension):
    from os.path import splitext
    return splitext(fn)[0]+'.' + extension


def prepare_jointwork(s):
    if len(s)==0:
        return s
    else:
        return '\\\\\n\\vspace{{0.5cm}}{0}'.format(s)

metadata = {'title':'Unspecified title', 'author':'', 'institution':'', 'workshop':'', 'jointwork':'', 'preamble':'', 'aspect':'169'}

r_meta  = re.compile(r'^(title:|author:|institution:|workshop:|jointwork:|preamble:|aspect:)\s(.*)')
r_slide = re.compile('##\s*(.*)')
r_section = re.compile('#\s*(.*)')

section_slide="""\\begin{{frame}}
\\Large
\\color{{darkred}}
{}
\\vskip-8pt
\\noindent\\makebox[\\linewidth]{{\\rule{{\\textwidth}}{{1pt}}}}
\end{{frame}}"""

def run(fn_input, fn_output, graphic_path=''):
    fs = IStream(fn_input)
    with open(fn_output, 'w') as fs_out:
        # look for metadata at the beginning of the file
        while not fs.eof():
            line = fs.peek()
            m_meta  = r_meta.match(line)
            m_slide = r_slide.match(line)
            m_section = r_section.match(line)
            if m_meta:
                fs.read()
                key = m_meta.group(1)[0:-1]
                if key in metadata.keys():
                    metadata[key] = m_meta.group(2)
                    print('added header entry: ', m_meta.group(1))
                else:
                    print('WARNING unknown header key: ', m_meta.group(1))

            elif line.strip() == '':
                fs.read() # ignore empty lines
            elif m_slide or m_section:
                break
            else:
                print('WARNING ignoring non empty line in header: ', line)
                fs.read()

        if os.path.isfile('template.tex'):
            template_file = 'template.tex'
        else:
            template_file = os.path.join(os.path.dirname(os.path.realpath(__file__)),'template.tex')
        header = open(template_file).read().format(metadata["title"], metadata["author"], metadata["institution"], metadata["workshop"], prepare_jointwork(metadata["jointwork"]))
        if graphic_path == '':
            header = header.replace('____GRAPHICPATH____', '')
        else:
            header = header.replace('____GRAPHICPATH____', '\\graphicspath{{' + graphic_path + '/}}')
        header = header.replace('____PREAMBLE____', metadata["preamble"])
        header = header.replace('____ASPECTRATIO____', metadata["aspect"])
        fs_out.write(header)

        # process the slides
        while not fs.eof():
            line = fs.peek()
            m_slide = r_slide.match(line)
            m_section = r_section.match(line)
            if m_slide:
                fs.read()
                fs_out.write( parse_slide(fs, m_slide.group(1)) )
            elif m_section:
                fs.read()
                fs_out.write(section_slide.format(m_section.group(1)))
            elif line.strip() != '':
                fs.read()
                print('WARNING ignoring non empty line: ', line)
            else: # empty string
                fs.read()

        fs_out.write('\n\\end{document}')



if len(sys.argv) != 2:
    print('ERROR: exactly one argument (the input file) is allowed')
    sys.exit(1)

fn_input = sys.argv[1]
fn_output = replace_file_ext(fn_input, 'tex')
run(fn_input, fn_output)
